/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LIBRARY.LIBRARY.controllers.books;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author albert
 */
public class BookRepository {

    private final Connection connection;

    public BookRepository() {
        connection = (new MysqlConnection()).getConnection();
    }

    public List<Book> get() throws SQLException {
        List books = new ArrayList();

        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * FROM books");

        while (rs.next()) {
            Book book = getBookData(rs);
            books.add(book);
        }

        return books;
    }

    /**
     * Find a record by id.
     *
     * @param id
     * @return
     * @throws SQLException
     */
    public Book find(int id) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * FROM books where id = " + id);

        if (rs.next()) {
            Book book = getBookData(rs);

            return book;
        }

        return null;
    }

    private Book getBookData(ResultSet rs) throws SQLException {
        Book book = new Book();
        book.setId(rs.getInt("id"));
        book.setAuthor(rs.getString("author"));
        book.setIsbn(rs.getString("isbn"));
        book.setName(rs.getString("name"));
        book.setNumberOfPages(rs.getInt("number_of_pages"));
        book.setDescription(rs.getString("description"));
        book.setPrice(rs.getDouble("price"));

        return book;
    }
}
