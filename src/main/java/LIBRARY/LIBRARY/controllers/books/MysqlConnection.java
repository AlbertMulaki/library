/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LIBRARY.LIBRARY.controllers.books;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author albert
 */
public class MysqlConnection {

    public static final String DATABASE_URL = "jdbc:mysql://localhost:3306/library?verifyServerCertificate=false&useSSL=false";
    public static final String root = "root";
    public static final String password = "";
    Connection connection = null;

    public Connection getConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.err.println("JDBC driver not found! Exit ...");
            System.exit(1);
        }
        try {
            Connection conn = DriverManager.getConnection(DATABASE_URL, root, password);
            System.out.println("CONNECTED SUCCESSFULLY");
            return conn;
        } catch (SQLException ex) {
            throw new RuntimeException("Error during accesing", ex);
        }
        
        
        
    }

}
