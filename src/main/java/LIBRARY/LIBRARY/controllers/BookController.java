/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LIBRARY.LIBRARY.controllers;

import LIBRARY.LIBRARY.controllers.books.BookRepository;
import java.sql.SQLException;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class BookController {
    @GetMapping("/books")
    public String index(Model model) throws SQLException {
        List books = (new BookRepository()).get();
        
        model.addAttribute("books", books);
        
        return "books/index";
    }
    
    @GetMapping("/books/{id}")
    public String show(Model model, @PathVariable("id") int id) throws SQLException {
        
        model.addAttribute("book", (new BookRepository()).find(id));
        
        return "books/show";
    }
}
